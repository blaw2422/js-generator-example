var Promise = require('bluebird');

function* demo3() {
  // simulate not exactly knowing when something will stop..
  // but knowing what the state will look like when its time to stop
  // instead of 'i < 10' - could be something like 'knex.raw().then(res => res.length === 0);'

  var i = 0;
  while ( i < 10 ) {
    // show how something taking some time will return when its "ready"
    yield Promise.resolve(i).delay(i * 1000).then((i) => { return i; });
    i++;
  }
}

function runDemo3() {
  
  // get generator
  var g = demo3();

  // calling 'next' will give us the first one.  
  // result will have 2 props
  // - value : yielded value from generator
  // - done : boolean of whether or not something was yielded ( how we know its finished )
  // go until the generator says its done
 
   Promise.each(g, a => {
    console.log(a);
  }, { concurrency: 1 }).then(() => {
    console.log('done');
  });
}

runDemo3();
