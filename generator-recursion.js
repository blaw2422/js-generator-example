var Promise = require('bluebird');

function* demo3() {
  var i = 0;
  while ( i < 5 ) {
    yield Promise.resolve(i).delay(i * 500).then((i) => { return i; });
    i++;
  }
}

function demoSingle(generator) {
  var a = generator.next();
  if (! a.done) {
    a.value.then(p => {
      console.log(p);
      demoSingle(generator);
    });
  } 
}

function runDemo4() {
  var g = demo3();
  demoSingle(g);
}

runDemo4();
